# README #

This Challenge was made by using **Vue.js** and **Node.js**.

This is the first time I work with this kind of system, so I don't have all the knowledges and experience about it. 

### What is this repository for? ###

This repo was developed as a challenge from **City Storage Systems | CloudKitchens**.

The way I did to solve this challenge was:

1. Read the JSON file from the server.
1. Fetch the information into the Client.
    - Define every order as `Ready` (Status = 0) which means that 
    - Populate every shelf (`Hot`, `Cold`, `Frozen`) with every order from the list until its maximum capacity: **15 items**.
  - When a shelf got full, then I placed more items into an `Overflow` shelf, which maximum capacity is **20 items**.
1. Once an order is placed into a shelf, then its status changes from `0` to `1`, which means the order has been `Placed` (into a shelf).
    - Also, it's set the Driver ETA using a random number between 2 and 10.
1. Every second that the order is on the shelf, it is calculated it's value by using the following formula: `shelfLife - orderAge - decayRate * orderAge`
1. If an order hasn't been picked up:
    - If it's value is higher than 20 then the card color will be **Blue** (`Status = 0` which means the order has been **Placed**).
  - If it's value is lower than 20 but higher than 10 then the card color will be **Yellow** (`Status = 2` which means **Warning** or mid risk of **Waste**).
  - If it's value is lower than 10 but higher than 0 then the card color will be **Red** (`Status = 3` which means **Danger** or high risk of **Waste**).
  - If it's value is lower or equal than 0 then the card color will be **Black** (`Status = 4` which means **Waste**).
1. If the order has been picked up by the **Driver**, then the `Status` will change to `1` and the card color will be **Green** (which means **Picked Up**).
1. If an order has been **Picked Up** or if it was marked as **Waste**, then it will be removed from its shelf.
    - When an order is removed from the shelf, then I'll look for another order of the same temperature into the **Overflow** shelf in order to move it for delivering.
  - When an order has been moved from **Overflow** to a temperature shelf, then I look for another pending order into the **Orders** array.

### How do I get set up? ###

1. Install Vue.js CLI
    - yarn global add @vue/cli
1. Install Node.js
    - You can go to [NodeJS Download Page](https://nodejs.org/en/) to download the Recommended version.
1. Clone the repository
1. Use `yarn install` to get all the required modules.
1. Open a terminal on the folder where you cloned the repo and type: `npm run start`
1. Open another terminal on the folder where you cloned the repo and type: `npm run server`.
1. Open a browser on `http://localhost:8085/`.

NOTE: Once you open the browser, it will run automatically.

