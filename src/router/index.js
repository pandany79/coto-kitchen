import Vue from 'vue'
import Router from 'vue-router'

import Orders from '@/components/Orders'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Orders,
    }
  ]
})

export default router
