import _ from 'lodash'

export const timer = {
  data () {
    return {
      intervals: {
        second: 1000,
        minute: 1000 * 60,
        hour: 1000 * 60 * 60
      }
    }
  },

  methods: {
    /**
     * Translate a given time in seconds to a string in format hh:mm:ss.
     * @param {Number} secs
     * @return {String}
     */
    secondsToTime (secs) {
      const hours = Math.floor(secs / this.intervals.hour)
      const minutes = Math.floor((secs % this.intervals.hour) / this.intervals.minute)
      const seconds = Math.floor((secs % this.intervals.minute) / this.intervals.second)
      const time = `${_.padStart(hours.toString(), 2, '0')}:${_.padStart(minutes.toString(), 2, '0')}:${_.padStart(seconds.toString(), 2, '0')}`
      return {
        hours,
        minutes,
        seconds,
        time
      }
    }
  }
}