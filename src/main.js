import Vue from 'vue'
import App from './App.vue'
import iView from 'view-design'
import locale from 'view-design/dist/locale/en-US'
import VueDOMPurifyHTML from 'vue-dompurify-html'
import VueCountdownTimer from 'vuejs-countdown-timer'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './router'

library.add(fas)
library.add(far)
library.add(fab)

Vue.component('font-awesome-icon', FontAwesomeIcon)

import './themes/index.less'
import './styles/colors.css'

Vue.use(iView, { locale })
Vue.use(VueDOMPurifyHTML, {
  default: {
    ADD_ATTR: ['target']
  }
})
// Vue.use(require('vue-moment'))
Vue.use(VueCountdownTimer)

Vue.config.productionTip = false

export default new Vue({
  router,
  components: { App },
  template: '<App/>',
  render: h => h(App)
}).$mount('#app')
