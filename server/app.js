const express = require('express');
const cors = require('cors')
const createError = require('http-errors');
const moment = require('moment');
const ordersRouter = require('./routes/orders-rest')

const allowedOrigins = ['http://localhost:8085', 'http://192.168.5.121:8085']
const getCurrentDateTime = () => {
  return `${moment(new Date()).format('YYYY-MM-DD')} at ${moment(new Date()).format('HH:mm:ss')}`
}
const corsOptions = {
  origin (origin, callback) {
    if (allowedOrigins.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      const currentDateTime = getCurrentDateTime()
      callback(new Error(`${currentDateTime} - Not allowed by CORS`));
    }
  },
  credentials: true,
};

const app = express()
const port = 3000
const host = '0.0.0.0'

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/orders', cors(corsOptions), ordersRouter.getOrders);
app.options('/orders', cors(corsOptions), ordersRouter.getOrders);

app.listen(port, host, () => {
  console.log(`Kitchen app listening on port ${port}!`)
})

// // view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

// app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', indexRouter);
// app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// // error handler
// app.use(function(err, req, res) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// // module.exports = app;
