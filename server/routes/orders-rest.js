const fs = require('fs');
var moment = require('moment');

/**
 * Fetches the existing orders in orders.json file.
 */
exports.getOrders = (req, res, next) => {
  fs.readFile(
    __dirname + '/../orders.json',
    (err, data) => {
      const currentDateTime = `${moment(new Date()).format('YYYY-MM-DD')} at ${moment(new Date()).format('HH:mm:ss')}`
      if (err) {
        console.error(`${currentDateTime} - Error while reading orders file`);
        next(err);
      } else {
        console.info(`Info: ${currentDateTime} - Fetched data from orders file`);
        const orders = JSON.parse(data.toString())
        res.json(orders);
      }
    }
  )
}
